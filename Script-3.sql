--1
select count(*) as "Total Assessments"from tlcprogram.assessments a join tlcprogram.student_assessments sa on sa.assessment_id = a.assessment_id where sa.assessment_date > (current_date - 30) and sa.assessment_date<current_date  ;

--2

select a.assessment_name , a.assessment_description, max(sa.assessment_score), min(sa.assessment_score) from tlcprogram.student_assessments sa join tlcprogram.assessments a on sa.assessment_id = a.assessment_id group by a.assessment_name,  a.assessment_description  ;

--3
select s.first_name, sa.assessment_grade, sa.assessment_id, a.assessment_name  from tlcprogram.student_assessments sa join tlcprogram.students s  on sa.student_id  = s.student_id join 
tlcprogram.assessments a on sa.assessment_id = a.assessment_id 
where sa.assessment_grade = 'F' and a.assessment_name  like 'Database%';

--4
select student_id ,sa.assessment_grade, count(*) from tlcprogram.student_assessments sa group by student_id, sa.assessment_grade having sa.assessment_grade = 'F';

select  * from student_assessments sa; 