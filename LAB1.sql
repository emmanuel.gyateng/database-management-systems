select *  from pubs2.titles;
-- Finding all titles with undefined price from the titles table.
select t.title, t.price  from pubs2.titles t where t.price is null;

--Finidng all titles with undefined price in the titles tables and changing it to $20.
select t.title, coalesce(t.price, money(20))  from pubs2.titles t where t.price is null;

--OR
select t.title, case 
when t.price is null then money(20) else t.price end as "Price" from pubs2.titles t where t.price is null;

-- Listing the first 50 Characters of pr_info of pub_info table
select substring(pi2.pr_info,0,50) as "First 50 Characters" from pubs2.pub_info pi2;

--Casting date to varchar using the CAST function
select cast(t.pubdate as varchar) as "Date to String" from pubs2.titles t;

--Casting date to varchar using the to_char function
select s.ord_date, to_char(s.ord_date,'Day, DDth Month YYYY') from  pubs2.sales s;

-- Current Date
select current_date;
-- Current Time
select current_time;
--Current Timestamp
select current_timestamp;
--Selecting  and casting to DATE
select cast('2018-09-26' as DATE);
--Selecting and converting to timestamp
select cast('2018-09-26' as TIMESTAMP);

--Subtracting from dates to know their diffrences
select DATE('2018-12-25') - DATE('2018-09-26');

--selecting the elapse date since publications of the books.
select t.title ,(current_date-t.pubdate) as "Elapse Days" from pubs2.titles t;
select t.title, concat((current_date-t.pubdate),' days') as "Elapse Days" from pubs2.titles t;

-- extracting the components from date objects

select t.title, extract (year from t.pubdate) as YEAR from pubs2.titles t;
select t.title, extract (day from t.pubdate)as MONTH from pubs2.titles t;

--Subtracting from dates to know their diffrences
select DATE('2018-03-02')-DATE('2018-02-01');

--Excercises (1)
select s.title_id, to_char(s.ord_date, 'DD-MM-YY') as "ORIGINAL DATE",s.ord_date +(current_date-DATE('2011-01-01'))  as "NEW DATE" from pubs2.sales s;

--Excercises (2) -- CHRISTMAS
select DATE('12-25-2022')-current_date as "DAYS TO CHRISTMAS";

--Excercises (2) -- NEWYEAR
select DATE('01-01-2023')-current_date as "DAYS TO NEW YEAR";
--Excercises (3) -- AGE
select concat((current_date -DATE('10-06-2000')),' Days') as "AGE";
--Excercises (4) difference between the pubdate and the current date in days as an integer.
select t.title, (current_date - t.pubdate) as "Difference DATE"  from pubs2.titles t ;
--Excercises (5) convert the date field to dd/mm/yy format
select to_char(s.ord_date,'DD/MM/YY')  from pubs2.sales s; 
--Excercises (6) select from the sales table stor_id for sales on your birthday in "dd/mm/yy" format.
select s.stor_id from pubs2.sales s where s.ord_date = DATE('09/13/94') ; 
