--Get average prices from the titles table for each type of book, and convert type to char(30).
select cast(t.type  as char(30)), avg(t.price::numeric) from pubs2.titles t group by 1;

--Print the difference between (to a resolution of days) the earliest and latest publication date in titles
select max(t.pubdate)-min(t.pubdate) as "RESOLUTION OF DAYS" from pubs2.titles t;
select age(max(t.pubdate),min(t.pubdate)) as "RESOLUTION OF DAYS" from pubs2.titles t;

--Print the average, min and max book prices within the titles table organised into groups based on type and publisher id.
select t.pub_id, avg(t.price::numeric) as "AVERAGE", min(t.price) as "MINIMUM", max(t.price) as "MAXIMUM" from pubs2.titles t group by t.pub_id, t.type;


-- Refine the previous question to show only those types whose average price is > $20 and output the results sorted on the average price.
select t.pub_id, avg(t.price::numeric) as "AVERAGE", min(t.price) as "MINIMUM", max(t.price) as "MAXIMUM" from pubs2.titles t group by t.pub_id,t.type having avg(t.price::numeric) > 20 order by 2;

--List the books in order of the length of their title
select t.title, length(t.title) from pubs2.titles t order by 2;

--What is the average age in months of each type of title?
select t.title,"type" , avg((current_date-t.pubdate)/12) from pubs2.titles t group by 2,1;

--How many authors live in each city?
select a.city, count(a.city) as "COUNT OF AUTHORS" from pubs2.authors a group by 1;

--What is the longest title?
select t.title, length(t.title) as "LENGTH" from pubs2.titles t order by 2 desc fetch first 1 rows only;

--How many books have been sold by each store and how many books have been sold in total?
select s.stor_id, sum(s.qty) as "TOTAL SALES" from pubs2.sales s group by 1;
select sum(s.qty) as "TOTAL SALES" from pubs2.sales s;


-- TABLE JOINS
--Join the publishers and pub_info and show the publisher name and the first 40 characters of the pr_info information.

select p.pub_name, substr(pi2.pr_info,1,40) as "PR_INFO" from pubs2.publishers p join pubs2.pub_info pi2 on p.pub_id =pi2.pub_id;

--Join the publishers and titles tables to show all titles published by each publisher. Display the pub_id, pub_name and  title_id.
select t.title,t.title_id,p.pub_id, p.pub_name from pubs2.publishers p join pubs2.titles t on p.pub_id= t.pub_id;

--For each title_id in the table titles, rollup the corresponding qty in sales  and show: title_id, title, ord_num, and the rolled-up value as a column aggregate called Total Sold
select s.title_id,t.title,s.ord_num, sum(s.qty) as "Total Sold" from pubs2.titles t join pubs2.sales s on t.title_id =s.title_id group by s.title_id,t.title,s.ord_num;

select * from pubs2.sales s;
select * from pubs2.authors a ;

--For each stor_id in stores, show the corresponding ord_num in sales and the discount type from table discounts. The output should consist of three columns: ord_num, discount and discounttype and should be sorted on ord_num
select s.ord_num, d.discount, d.discounttype,ord_num from pubs2.sales s join pubs2.discounts d on s.stor_id =d.stor_id order by s.ord_num;

--Show au_lname from authors, and pub_name from publishers when both publisher and author live in the same city.

select au_lname,pub_name,a.city  from pubs2.publishers p 
join pubs2.titles t on p.pub_id =t.pub_id
join pubs2.titleauthor t2 on t2.title_id = t.title_id
join pubs2.authors a on t2.au_id =a.au_id
where p.city = a.city;
--OR
select au_lname,pub_name,a.city  from pubs2.publishers p
join pubs2.authors a on p.city =a.city ;


--Refine 5 so that for each author you show all publishers who live in the same city and have published one of the authors titles.
select au_lname,pub_name,a.city  from pubs2.publishers p 
join pubs2.titles t on p.pub_id =t.pub_id
join pubs2.titleauthor t2 on t2.title_id = t.title_id
join pubs2.authors a on t2.au_id =a.au_id
where p.city = a.city;

-- Refine 1 so that an outer join is performed. All of the publishers from the first table should be shown, not just those with pr_info information in pub_info. You should use the ANSI SQL92 syntax.
select p.pub_name, substr(pi2.pr_info,1,40) as "PR_INFO" from pubs2.publishers p left outer join pubs2.pub_info pi2 on p.pub_id =pi2.pub_id;


--List each publisher's name, the title of each book they have sold and the total quantity of that title.
select pub_name, title, sum(qty) as "Total Quantity" from pubs2.publishers p join pubs2.titles t on p.pub_id =t.pub_id join  pubs2.sales s on t.title_id =s.title_id group by pub_name,title; 

--How many books have been published by each publisher?
select 










select * from pubs2.authors a;
select * from pubs2.publishers p 










